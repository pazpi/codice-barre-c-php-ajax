# Lettore codice a barre
Questo software è stato sviluppato da Lorenzo, Davide e Leonardo in un pomeriggio
uggioso, non è stato fatto assolutamente con l'intento di essere un programma 
completo, è stato creato per il puro scopo illustrativo e ludico dell'apparecchio.

## File C
Il programma in C permette di leggere i codice a barre e di aggiungerne di nuovi
al database, che per semplicità è stato implementato come un file di testo.

Per aggiungere un nuovo prodotto premere `a` prima della scansione e poi seguire
le indicazioni a schermo.
Per usicire dal programma premere `c`

## File PHP
PHP viene usato come engine per la comunicazione con il database e la gestione
delle richieste AJAX.
AJAX crea metodi `GET` a PHP che risponde tramite `echo` la descrizione del prodotto.

## File HTML
La pagina web è una normale pagina di login con i campi username e password cambiati,
al posto del nome utente si deve immettere il codice a barre, scannerizzato con 
la pistola; dopo la lettura del codice in automatico viene invitato un carattere
di `RETURN` e verrà mostrato nel campo password la descrizione del prodotto.

La pagina web è stata liberamente ispirata dal lavoro di Allie Kingsley e scaricata
dal sito [35 Free CSS3 HTML5 Login Form Templates](https://dcrazed.com/css-html-login-form-templates/)