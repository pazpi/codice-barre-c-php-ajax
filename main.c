#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_LEN 1024
#define MAX_PRODS 1024
#define BAR_LINDT_CB "3046920043489"

typedef struct str_prodottoType{
    char codice[MAX_LEN];
    char descr[MAX_LEN];
}prodottoType;

prodottoType prodotti[MAX_PRODS];
int nProds;


void loadProdotti(prodottoType *p, int *n)
{
    FILE *fp;
    char strAux[MAX_LEN];

    fp = fopen("lista.txt","r");

    *n = 0;
    /* printf("ciccio"); */
    while(fgets(strAux, MAX_LEN, fp))
    {
        printf("%s", strAux);
        sscanf(strAux, "%s %s",p->codice,p->descr);
        /* printf("Letto Codice: %s; Descr: %s\n",p->codice,p->descr); */

        (*n)++;
        p++;
    }
    fclose(fp);

    return;
}

int findProdotto(char *str, prodottoType *p, int n)
{
    int i;

    for(i = 0; i < n; i++)
    {
        if(strcmp(str,p->codice) == 0)
        {
            return i;
        }
        p++;
    }
    return -1;
}

void aggiungiProdotto(prodottoType *p, int *nProds){
    char newCode[MAX_LEN];
    char newDescr[MAX_LEN];

    printf("Nuovo Prodotto: \n");
    printf("\tScansiona nuovo codice: ");
    scanf("%s", &newCode);

    printf("\tInserisci nuova descrizione: ");
    scanf("%s", &newDescr);

    (*nProds)++;
    sprintf(p->codice, "%s", newCode);
    sprintf(p->descr, "%s", newDescr);

    return;
}

void salvaProdotti(prodottoType *p, int nProds){
    int i;
    FILE *fp;

    fp = fopen("lista.txt", "w");

    for(i=0;i<nProds;i++){
        fprintf(fp, "%s %s\n", p->codice, p->descr);
        p++;
    }
    fclose(fp);
}


int main(){
    char bar[MAX_LEN];
    int idx;

    loadProdotti(prodotti, &nProds);

    while(1){

        printf("Scansiona il tuo prodotto: ");

        scanf("%s", &bar);

        if(strcmp(bar, "q") == 0) break;

        if(strcmp(bar, "a") == 0) {
            aggiungiProdotto(&(prodotti[nProds]), &nProds);
            salvaProdotti(prodotti, nProds);
        }
        else{
            idx = findProdotto(bar, prodotti, nProds);
            if(idx >= 0){
                printf("idx: %d\n", idx);
                printf("Prodotto trovato: %s\n",prodotti[idx].descr);
            }
            else{
                printf("Altro\n");
            }
        }
        printf("\n\n");
    }
    return 0;
}
