MAIN = main.c

TARGET_MAIN = scanner

CFLAGS = 

all: $(TARGET_MAIN)

$(TARGET_MAIN):$(MAIN)
	gcc -o $(TARGET_MAIN) $(MAIN) $(CFLAGS)

clean:
	rm -f $(TARGET_MAIN)
